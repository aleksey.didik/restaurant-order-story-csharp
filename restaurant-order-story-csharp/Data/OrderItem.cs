﻿namespace RestaurantOrderStory.Data
{
    public abstract class OrderItem : IPriority
    {
        public string Name { get; set; }

        public int Priority { get; set; }

        public abstract void SetPriority();
    }
}
