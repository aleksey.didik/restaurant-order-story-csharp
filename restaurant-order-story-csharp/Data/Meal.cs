﻿namespace RestaurantOrderStory.Data
{
    public class Meal : OrderItem
    {
        public MealType Type { get; set; }

        public enum MealType
        {
            Snack,
            MainDish,
            Dessert,
            Salad,
            Entrance
        }

        public override void SetPriority()
        {
            switch (Type)
            {
                case MealType.Salad:
                case MealType.Entrance:
                    Priority = 1;
                    break;

                case MealType.MainDish:
                    Priority = 2;
                    break;

                case MealType.Snack:
                case MealType.Dessert:
                    Priority = 3;
                    break;
            }
        }
    }
}
