## Open Closed Principle

**To read**: [https://epa.ms/restaurant-order-story-csharp]

## Story Outline
This story will help you to understand the Open Closed Principle (OCP) using C# by adapting some existing code and make it follow OCP.

## Story Organization
**Story Branch**: master
>`git checkout master`

**Practical task tag for self-study**:task
>`git checkout task`

Tags: #solid, #principles, #open_closed
